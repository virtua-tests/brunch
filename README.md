![Build Status](https://gitlab.com/pages/brunch/badges/master/build.svg)

---

Example [Brunch] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation http://doc.gitlab.com/ee/pages/README.html.

---

## `.gitlab-ci.yml` contents

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: node:4.2.2

pages:
  cache:
    paths:
    - node_modules/

  script:
  - npm install -g brunch
  - brunch build --production
  artifacts:
    paths:
    - public
  only:
  - master
```

## Building locally 

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install](https://nodejs.org/en/download) Node.js
1. Install Brunch: `npm install -g brunch`
1. Install Brunch plugins and app dependencies: `npm install`
1. Generate the website: `brunch build --production`
1. Preview your project: `brunch watch --server`
1. Add content

Read more at Brunch's [documentation][].

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.
    
----

Forked from @VeraKolotyuk

[ci]: https://about.gitlab.com/gitlab-ci/
[brunch]: http://brunch.io
[install]: https://github.com/brunch/brunch/tree/master/docs
[documentation]: https://github.com/brunch/brunch/tree/master/docs
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
